# Taller: Dado un conjunto de valores en una lista de 1000 valores generados aleatoriamente entre 0 y 20.
# 1. Crear una función que devuelva una lista con valor True o False dependiendo si el valor de la lista original es primo o no.
# 2. Crear una función que devuelva la media de la lista.
# 3. Crear una función que devuelva una lista con el factorial de cada uno de los números.
# 4. Crear una función que retorne el valor mayor y el menor de la lista.
import random
import pandas as pd
from math import factorial

Lista=[]
ToF=[]
Factorial=[]

def primos(p):
    if p>1:
        for n in range(2,p):
            if p%n==0:
                return False
    else:
        return False
    return True 

def media():
    m=pd.Series(Lista)
    med=m.mean()
    return med

def fac(s):
    f=factorial(s)
    return f

def MayorMenor():
    s=pd.Series(Lista)
    mayor=s.max()
    menor=s.min()
    return print("El mayor es: ",mayor),print("El menor es: ",menor)
   
i=0
while(i<1000):
    x=random.randint(0,20)
    Lista.append(x) 
    ToF.append(primos(x))
    Factorial.append(fac(x))
    i=i+1

print(Lista)
print(ToF)
print("La media es: ",media())
print(Factorial)
print(MayorMenor())

# Crear una clase que implemente una calculadora aritmetica. 

class Calculadora:

    def __int__(self, a,b):
        self.num1=a
        self.num2=b

    def sumar(self):
        suma=self.num1+self.num2
        return suma

    def restar(self):
        resta=self.num1-self.num2
        return resta

    def multiplicar(self):
        multiplicacion=self.num1*self.num2
        return multiplicacion

    def dividir(self):
        division=self.num1/self.num2
        return division       

    def mod(self):
        modulo=self.num1%self.num2
        return modulo 

    def potenciar(self):
        potencia=pow(self.num1, self.num2)
        return potencia 

miCalculadora=Calculadora()
miCalculadora.num1=float(input("Ingrese el primer numero: "))
miCalculadora.num2=float(input("Ingrese el segundo numero: "))

print("El resultado de la suma es: ",miCalculadora.sumar())
print("El resultado de la resta es: ",miCalculadora.restar())
print("El resultado de la multiplicacion es: ",miCalculadora.multiplicar())
print("El resultado de la division es: ",miCalculadora.dividir())
print("El resultado del modulo es: ",miCalculadora.mod())
print("El resultado de la potenciacion es: ",miCalculadora.potenciar())
# Utilizando pandas en Python calcular la mediana y media de los datos almacenados en una lista de 100 elementos.
# La lista debe “llenarse” con números aleatorios de -100 a 100.
import pandas as pd
import random

Lista=[]

i=0
while(i<100):
    x=random.randint(-100,100)
    Lista.append(x) 
    i=i+1
print(Lista)

s=pd.Series(Lista)
mediana=s.median()
print("La mediana es: ",mediana)

media=s.mean()
print("La media es: ",media)